#include "sampling.hpp"

#include<cmath>

//===== sampling =====

namespace SamplingOptions {

  //seed the default engine with a random device seed
  void use_random_device() {
    std::random_device dev ;
    alea.seed(dev()) ;
  }
}

//-- 1D sampling --

int rand_index(int begin, int end) {
  std::uniform_int_distribution<int> rand_int(begin, end - 1) ;
  return rand_int(alea) ;
}

double rand_double() {
  std::uniform_real_distribution<double> rand_coord(0,1) ;
  return rand_coord(alea) ;
}

//-- 2D sampling --

//random point in the unit square
Point rand_in_square() 
{
  std::uniform_real_distribution<double> rand_coord(0,1) ;
  return Point({rand_coord(alea), rand_coord(alea)}) ;
}

//random point on the unit circle
Point rand_in_circle() 
{
  std::normal_distribution<> rand_coord ;
  Point result({rand_coord(alea), rand_coord(alea)}) ;
  double inv_norm = 1. / sqrt(result[0]*result[0] + result[1]*result[1]) ;
  return inv_norm * result ;
}

//random point in the unit disk
Point rand_in_disk() 
{
  std::uniform_real_distribution<double> rand_radius(0,1) ;
  return sqrt(rand_radius(alea)) * rand_in_circle() ;
}

//-- Colors --

static Color hsv_to_rgb(const Color& hsv)
{
  Color rgb ;

  double h = hsv[0] ;
  double s = hsv[1] ;
  double v = hsv[2] ;

  int hi = (int) h ;
  double hr = h - hi ;

  double l = v*(1-s) ;
  double m = v*(1-hr*s);
  double n = v*(1-(1-hr)*s);

  if(h<1) {
    rgb[0] = v ;
    rgb[1] = n ;
    rgb[2] = l ;
  } else if(h<2) {
    rgb[0] = m ;
    rgb[1] = v ;
    rgb[2] = l ;
  } else if(h<3) {
    rgb[0] = l ;
    rgb[1] = v ;
    rgb[2] = n ;
  } else if(h<4) {
    rgb[0] = l ;
    rgb[1] = m ;
    rgb[2] = v ;
  } else if(h<5) {
    rgb[0] = n ;
    rgb[1] = l ;
    rgb[2] = v ;
  } else {
    rgb[0] = v ;
    rgb[1] = l ;
    rgb[2] = m ;
  }

  return rgb ;
}

Color rand_bright_color() 
{
  std::uniform_real_distribution<double> rand_double(0,1) ;

  Color hsv ;
  //random hue
  hsv[0] = rand_double(alea) ;
  hsv[0] *= 6 ;
  //random saturation, with minimum at 0.5
  hsv[1] = rand_double(alea) ;
  hsv[1] = 0.7*hsv[1] + 0.3 ;
  //random value with minimum at 0.5
  hsv[2] = rand_double(alea) ;
  hsv[2] = 0.2*hsv[2]+0.8 ;

  //RBG conversion
  return hsv_to_rgb(hsv) ;
}
